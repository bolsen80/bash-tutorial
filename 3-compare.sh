#!/bin/bash

# Let's compare the outputs of the gzip and non-compressed file
# in size and see if the compressed data matches.

if [[ ! -e haystack.gz ]]; then
    ./2-compress.sh
fi

if [[ ! -e haystack ]]; then
    gunzip -k haystack.gz
fi


ls -lh haystack haystack.gz

# TODO: This doesn't match
cat haystack | sha256sum
gzip -kc haystack.gz | sha256sum

#!/bin/bash

# We will find a random place to put a file on a file system that we can put it in
# a temp file record the location, then after this we will try to find it in the filesystem.

temp=$(mktemp)
which_dir=$(echo $(( $RANDOM % 10000 + 1 )))
count=0
use_dir=
for directory in $(find / -user $USER -type d 2>/dev/null); do
    if [[ count -eq which_dir ]]; then
        use_dir=$directory
        echo $directory > $temp
        break
    fi
    count=$((count+1))
done
echo "Hint ..."
echo $temp
echo
cp haystack $use_dir

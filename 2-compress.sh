#!/bin/bash

# Let's generate the haystack but use a named pipe to feed data into gzip

mknod hay.pipe p
gzip < hay.pipe > haystack.gz &
./1-generate.sh > hay.pipe
rm hay.pipe

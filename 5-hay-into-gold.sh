#!/bin/bash

# Let's have some options to turn some hay into gold - keep the gold or just imagine it?

EDIT_IN_PLACE=0

usage() {
    cat <<EOF
$0 (-i) (-h)
  -i: Edit in place
  -h: help
EOF

exit 0
}

while getopts "ih" opt; do
    case $opt in
        i)
            EDIT_IN_PLACE=1
            ;;
        h)
            usage
            ;;
    esac
done


# TODO: explain quoting
if [[ "$EDIT_IN_PLACE" == "1" ]]; then
    sed -i 's/hay/gold/g' haystack
else
    sed 's/hay/gold/g' haystack
fi

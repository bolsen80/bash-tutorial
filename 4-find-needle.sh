#!/bin/bash

# Let's find that needle in two different ways, one in the uncompressed version,
# the other in the compressed version.
#
# Let's also see which is faster :)

bold=$(tput bold)
normal=$(tput sgr0)

if [[ ! -e haystack.gz || ! -e haystack ]]; then
    echo "Both haystack and haystack.gz must exist. Run 2-compress.sh." 2>&1
    exit 1
fi

echo "${bold}Using grep ...${normal}"
time grep needle haystack

echo

echo "${bold}Using zgrep ...${normal}"
time zgrep needle haystack.gz

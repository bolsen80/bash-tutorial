#!/bin/bash

# Let's create a big file and fill it up with lines of "hay"
# but in one line add "needle".

needle_line=$(echo $(( $RANDOM % 10000 + 1 )))
for i in $(seq 1 10000); do
    echo -n "$i: "

    if [[ $needle_line -eq $i ]]; then
        echo "needle"
        continue
    fi

    for j in $(seq 1 10); do
        echo -n "hay "
    done

    echo
done

# Bash tutorial

I found this idea from a presentation on YouTube (link to be found) about things you can do in a Unix operating system, with using a "needle-in-a-haystack" as an example.

This goes through increasingly odd things with this file, illustrating along the way random things that can be done in bash and other utilities.

This also forces me to learn more about stuff, so I have to try new things.

# Plan

 + [X] 1-generate.sh: Create the file
 + [X] 2-compress.sh: Compress it
 + [X] 3-compare.sh: Compare the compressed and non-compressed files
 + [X] 4-find-needle.sh: Simple way to find it
 + [X] 5-hay-into-gold.sh: Use `sed` to change hay into gold
 + [X] 6-bury-the-haystack.sh: Find a random directory and dump the haystack there
 + [ ] 7-guess-where.sh: Regenerate haystack, get 5 chances with "warmer" or "colder" and "you win" if you figure out correct location
 + [ ] 8-templating.sh: We need to make a quick templating tool for the next step ...
 + [ ] 9-guess-web.sh: Just like #7 but as a web page and CGI (but definitely not for critical applications!)
 + [ ] 10-web-analytics.sh: Let's create a report from the web server log!
